#prerequisite
anaconda with python3 :https://www.anaconda.com/download/ 

# installation

* Setup env with required dependencies 
`conda env create -f environment.yml`


* use environment 
`conda activate datascienceoec`


* lauch jupyter notebook 
`jupyter notebook`

